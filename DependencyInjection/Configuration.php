<?php

namespace XLabs\LikeBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('x_labs_like');

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.
        $rootNode
            ->children()
                ->arrayNode('redis_settings')->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('host')->defaultValue('127.0.0.1')->end()
                        ->integerNode('port')->defaultValue(6379)->end()
                        ->integerNode('database_id')->defaultValue(6)->end()
                    ->end()
                ->end()
                ->scalarNode('_key_namespace')->defaultValue('xlabs:like')->end()
                ->scalarNode('_star_key_namespace')->defaultValue('xlabs:star')->end()
                //->booleanNode('star_system')->defaultFalse()->end()
                ->arrayNode('backup')
                    ->prototype('scalar')->end()
                ->end()
                ->arrayNode('star_rating')->addDefaultsIfNotSet()
                    ->children()
                        ->integerNode('amount')->defaultValue(5)->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
