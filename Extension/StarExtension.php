<?php

namespace XLabs\LikeBundle\Extension;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Templating\EngineInterface;
use XLabs\LikeBundle\Engines\Star as StarEngine;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class StarExtension extends AbstractExtension
{
    private $token_storage;
    private $templating;
    private $xlabs_star_engine;
    private $config;

    public function __construct(TokenStorageInterface $token_storage, EngineInterface $templating, StarEngine $xlabs_star_engine, $config)
    {
        $this->token_storage = $token_storage;
        $this->templating = $templating;
        $this->xlabs_star_engine  = $xlabs_star_engine;
        $this->config  = $config;
    }
    
    public function getFunctions()
    {
        return array(
            new TwigFunction('renderStars', array($this, 'renderStars'), array(
                'is_safe' => array('html') // to render as raw html
            )),
            new TwigFunction('getStarAverage', array($this, 'getStarAverage')),
            new TwigFunction('getUserStarRating', array($this, 'getUserStarRating')),
            new TwigFunction('getTotalStarRatings', array($this, 'getTotalStarRatings')),
            new TwigFunction('getMostRated', array($this, 'getMostRated')),
            new TwigFunction('getMostRatedByUser', array($this, 'getMostRatedByUser')),
        );
    }
    
    public function getFilters()
    {
        return array();
    }

    public function renderStars($ratedType, $rated_id, $value = false, $force_readonly = false, $js_callback = false)
    {
        return $this->templating->render('XLabsLikeBundle:Star:widget.html.twig', array(
            'stars_id' => md5(json_encode(array($ratedType, $rated_id, $value, $force_readonly, $js_callback))),
            'ratedType' => $ratedType,
            'rated_id' => $rated_id,
            'value' => $value, // the current user rating
            'force_readonly' => $force_readonly,
            'js_callback' => $js_callback,
            'total_stars' => $this->config['star_rating']['amount']
        ));
    }

    public function getStarAverage($ratedType, $rated_id)
    {
        return $this->xlabs_star_engine->getAverageRating($ratedType, $rated_id);
    }

    public function getUserStarRating($ratedType, $rated_id)
    {
        return $this->xlabs_star_engine->getUserRating($ratedType, $rated_id);
    }

    public function getTotalStarRatings($ratedType, $rated_id)
    {
        return $this->xlabs_star_engine->getTotalRatings($ratedType, $rated_id);
    }

    public function getMostRated($ratedType, $maxResults = false, $page = 1, $showScores = false)
    {
        return $this->xlabs_star_engine->getMostRated($ratedType, $maxResults, $page, $showScores);
    }

    public function getMostRatedByUser($ratedType)
    {
        return $this->xlabs_star_engine->getMostRatedByUser($ratedType);
    }
}