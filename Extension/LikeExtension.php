<?php

namespace XLabs\LikeBundle\Extension;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Templating\EngineInterface;
use XLabs\LikeBundle\Engines\Like as LikeEngine;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class LikeExtension extends AbstractExtension
{
    private $token_storage;
    private $templating;
    private $xlabs_like_engine;

    public function __construct(TokenStorageInterface $token_storage, EngineInterface $templating, LikeEngine $xlabs_like_engine)
    {
        $this->token_storage = $token_storage;
        $this->templating = $templating;
        $this->xlabs_like_engine  = $xlabs_like_engine;
    }
    
    public function getFunctions()
    {
        return array(
            new TwigFunction('checkLike', array($this, 'checkLike')),
            new TwigFunction('getLikes', array($this, 'getLikes')),
        );
    }
    
    public function getFilters()
    {
        return array();
    }

    public function checkLike($likedType, $liked_id)
    {
        $user = $this->token_storage->getToken()->getUser();
        return is_string($user) ? false : $this->xlabs_like_engine->likes($likedType, $liked_id);
    }

    public function getLikes($likedType, $liked_id)
    {
        return $this->xlabs_like_engine->getTotalLikes($likedType, $liked_id);
    }
}