<?php

namespace XLabs\LikeBundle\Queue;

use XLabs\RabbitMQBundle\RabbitMQ\Consumer as Parent_Consumer;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use XLabs\LikeBundle\Entity\Like;
use XLabs\LikeBundle\Entity\StarRating;
use \DateTime;
use \Exception;

class Consumer extends Parent_Consumer
{
    // set your custom consumer command name
    protected static $consumer = 'xlabs:like:mysql_backup';

    // following function is required as it is
    protected function configure()
    {
        $this
            ->setName(self::$consumer)
        ;
    }

    // following function is required as it is
    public function getQueueName()
    {
        return Producer::getQueueName();
    }

    public function callback($msg)
    {
        // this is all sample code to output something on screen
        $container = $this->getApplication()->getKernel()->getContainer();

        $msg = json_decode($msg->body);

        $action = $msg->action;
        $user_id = $msg->user_id;
        $liked_type = $msg->liked_type;
        $liked_id = $msg->liked_id;
        $score = $msg->score;

        //$em = $container->get('doctrine.orm.default_entity_manager');
        $em = $this->getEntityManager();
        switch($action)
        {
            case 'like':
                $now = new DateTime();
                $score = $now->setTimestamp($score);
                $like = new Like($user_id, $liked_type, $liked_id, $score);
                $em->persist($like);
                break;
            case 'unlike':
                $like = $em->getRepository(Like::class)->findOneBy(array(
                    'liked_id' => $liked_id,
                    'liked_type' => $liked_type,
                    'user_id' => $user_id
                ));
                if($like)
                {
                    $em->remove($like);
                }
                break;
            case 'rating':
                $rating = new StarRating($user_id, $liked_type, $liked_id, $score);
                $em->persist($rating);
                break;
        }
        try {
            $em->flush();
        } catch (Exception $e) {

        }
    }

    private function getEntityManager()
    {
        $container = $this->getApplication()->getKernel()->getContainer();
        $em = $container->get('doctrine.orm.default_entity_manager');
        if(!$em->isOpen())
        {
            $em = $em->create($em->getConnection(), $em->getConfiguration());
        }
        return $em;
    }
}