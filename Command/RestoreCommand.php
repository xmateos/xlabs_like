<?php

namespace XLabs\LikeBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use XLabs\LikeBundle\Engines\Like as LikeEngine;
use XLabs\LikeBundle\Engines\Star as StarEngine;
use XLabs\LikeBundle\Entity\Like;
use XLabs\LikeBundle\Entity\StarRating;
use Symfony\Component\Console\Helper\ProgressBar;

class RestoreCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('xlabs:like:restore')
            ->setDescription('Puts all MySQL data into redis. Optional user ids to restore only theirs.')
            ->addArgument('user_ids', InputArgument::IS_ARRAY, 'User ID')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getApplication()->getKernel()->getContainer();
        $em = $container->get('doctrine.orm.default_entity_manager');

        $like_engine = $container->get(LikeEngine::class);
        $star_engine = $container->get(StarEngine::class);

        $qb = $em->createQueryBuilder();
        $qb
            ->select('e')
            ->from(Like::class, 'e');

        $user_ids = $input->getArgument('user_ids') ? $input->getArgument('user_ids') : false;
        if($user_ids)
        {
            $qb->where(
                $qb->expr()->in('e.user_id', $user_ids)
            );
        }
        $entities = $qb->getQuery();

        $output->writeln('');
        $output->writeln('<comment>::::::::::::::: RESTORING LIKES :::::::::::::::</comment>');
        $output->writeln('<comment>::::::::::::::: MySQL --> Redis :::::::::::::::</comment>');
        $output->writeln('');

        $progress = new ProgressBar($output, count($entities->getArrayResult()));
        $progress->start();
        $progress->setMessage('Likes');
        $progress->setFormat('[<info>%bar%</info>] %current%/%max% <comment>%message%</comment> %percent:3s%%');
        foreach($entities->iterate() as $like)
        {
            $like = $like[0];
            $like_engine->setUser(array('id' => $like->getUserId()))->like($like->getLikedType(), $like->getLikedId(), $like->getScore()->getTimestamp(), false);
            $progress->advance();
        }
        $progress->finish();

        $qb = $em->createQueryBuilder();
        $qb
            ->select('e')
            ->from(StarRating::class, 'e');

        $user_ids = $input->getArgument('user_ids') ? $input->getArgument('user_ids') : false;
        if($user_ids)
        {
            $qb->where(
                $qb->expr()->in('e.user_id', $user_ids)
            );
        }
        $entities = $qb->getQuery();

        $output->writeln('');
        $output->writeln('<comment>:::::::::::: RESTORING STAR RATINGS ::::::::::::</comment>');
        $output->writeln('<comment>::::::::::::::: MySQL --> Redis :::::::::::::::</comment>');
        $output->writeln('');

        $progress = new ProgressBar($output, count($entities->getArrayResult()));
        $progress->start();
        $progress->setMessage('Star ratings');
        $progress->setFormat('[<info>%bar%</info>] %current%/%max% <comment>%message%</comment> %percent:3s%%');
        foreach($entities->iterate() as $rating)
        {
            $rating = $rating[0];
            $star_engine->setUser(array('id' => $rating->getUserId()))->rate($rating->getRatedType(), $rating->getRatedId(), $rating->getScore(), false);
            $progress->advance();
        }
        $progress->finish();

        $output->writeln('');
        $output->writeln('<comment>::::::::::::::::::::::: DONE ::::::::::::::::::::::</comment>');

        /*$likes = $em->getRepository(Like::class)->findAll();
        if($likes)
        {
            foreach($likes as $like)
            {
                $like_engine->setUser(array('id' => $like->getUserId()))->like($like->getLikedType(), $like->getLikedId(), $like->getScore()->getTimestamp());
            }
        }*/
    }
}