<?php

namespace XLabs\LikeBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use XLabs\LikeBundle\Engines\Like as LikeEngine;
use XLabs\LikeBundle\Entity\Like;
use Symfony\Component\Console\Helper\ProgressBar;
use \DateTime;
use \Exception;

class InitialBackupCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('xlabs:like:initial_backup')
            ->setDescription('Puts all redis data into MySQL')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getApplication()->getKernel()->getContainer();
        $em = $container->get('doctrine.orm.default_entity_manager');

        $output->writeln('');
        $output->writeln('<comment>:::::::::::::::  BACK UP LIKES  :::::::::::::::</comment>');
        $output->writeln('<comment>::::::::::::::: Redis --> MySQL :::::::::::::::</comment>');
        $output->writeln('');

        $bundle_config = $container->getParameter('xlabs_like_engine');
        if(array_key_exists('backup', $bundle_config) && is_array($bundle_config['backup']) && count($bundle_config['backup']))
        {
            $likeable_entities = $bundle_config['backup'];
        } else {
            dump('No entities to be backed up found on the bundle configuration.');
            exit;
        }

        $like_engine = $container->get(LikeEngine::class);

        $likes_counter = 0;
        foreach($likeable_entities as $key => $likeable_entity)
        {
            $qb = $em->createQueryBuilder();
            $entities = $qb
                ->select('e')
                ->from($likeable_entity, 'e')
                ->getQuery();
            $progress = new ProgressBar($output, count($entities->getArrayResult()));
            $progress->start();
            $progress->setMessage('Redis Likes ['.$likeable_entity.']');
            $progress->setFormat('[<info>%bar%</info>] %current%/%max% <comment>%message%</comment> %percent:3s%%');
            foreach($entities->iterate() as $e)
            {
                $e = $e[0];

                // Get likers
                $likers = $like_engine->getLikers($key, $e->getId(), $showScores = true, $maxResults = false);
                if(count($likers))
                {
                    foreach($likers as $liker_id => $score)
                    {
                        $now = new DateTime();
                        $score = $now->setTimestamp($score);
                        $like = new Like($liker_id, $key, $e->getId(), $score);
                        $em->persist($like);
                        $likes_counter++;
                        /*try {
                            $em->flush();
                        } catch(Exception $exception) {

                        }*/
                    }
                }

                if($likes_counter % 100 == 0)
                {
                    $em->flush();
                    $em->clear();
                }

                $progress->advance();
            }
            $em->flush();

            $progress->finish();
        }
    }
}