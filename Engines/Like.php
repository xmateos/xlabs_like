<?php

namespace XLabs\LikeBundle\Engines;

use Predis\Client as Predis;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use XLabs\LikeBundle\Queue\Producer as MysqlBackup;
use XLabs\LikeBundle\Entity\User;

class Like
{
    private $config;
    private $redis;
    protected $user_in_session;
    private $mysql_backup;

    public function __construct($config, TokenStorageInterface $token_storage, MysqlBackup $mysql_backup)
    {
        $this->config = $config;
        $this->redis = new Predis(array(
            'scheme' => 'tcp',
            'host'   => $config['redis_settings']['host'],
            'port'   => $config['redis_settings']['port'],
            'database' => $config['redis_settings']['database_id']
        ), array(
            'prefix' => isset($config['_key_namespace']) ? $config['_key_namespace'].':' : ''
        ));
        $this->user_in_session = $token_storage->getToken() ? $token_storage->getToken()->getUser() : false;
        $this->mysql_backup = $mysql_backup;
    }

    public function disableLogging()
    {
        /*
         * For bulk operations, call this method to avoid PHP memory leaks
         */
        $this->redis->getConnection()->setLogger(null);
        return $this;
    }

    public function getInstance()
    {
        return $this->redis;
    }

    public function getAllKeys($redis_instance = false)
    {
        $redis_instance = $redis_instance ? $redis_instance : $this->redis;
        $keyPattern = "*";
        $keys = $redis_instance->keys($keyPattern);
        return $keys;
    }

    public function deleteAllKeys($redis_instance = false)
    {
        $redis_instance = $redis_instance ? $redis_instance : $this->redis;
        $keys = $this->getAllKeys();
        if($keys)
        {
            $prefix = $redis_instance->getOptions()->__get('prefix')->getPrefix();
            foreach($keys as $key)
            {
                $key = substr($key, strlen($prefix));
                $redis_instance->del(array($key));
            }
        }
    }

    public function changePrefix($old_prefix, $new_prefix)
    {
        $redis['generic'] = new Predis([
            'scheme' => 'tcp',
            'host'   => $this->config['redis_settings']['host'],
            'port'   => $this->config['redis_settings']['port'],
            'database' => $this->config['redis_settings']['database_id']
        ]);
        $keys = $this->getAllKeys($redis['generic']);
        if($keys)
        {
            foreach($keys as $key)
            {
                if(strlen(strstr($key, $old_prefix)) > 0)
                {
                    $redis['generic']->rename($key, str_replace($old_prefix, $new_prefix, $key));
                }
            }
        }
        $redis['generic']->disconnect();
    }

    public function setUser($user)
    {
        if(is_array($user))
        {
            $u = $user;
            $user = new User();
            $user->id = $u['id'];
        }
        $this->user_in_session = $user;
        return $this;
    }

    /*
     * Make user like $likedType
     * $likedType: another user, gallery, ... liked entity
     * $liked_id: liked entity id
     * $backup: will only be false when restoring from mysql to redis
     */
    public function like($likedType, $liked_id, $score = false, $backup = true)
    {
        $score = $score ? $score : time();
        $this->redis->zAdd("user:".$this->user_in_session->getId().":likes:".$likedType, array($liked_id => $score));
        $this->redis->zAdd($likedType.":".$liked_id.":liked:user", array($this->user_in_session->getId() => $score));
        $this->redis->zIncrBy("ranking:".$likedType, 1, $liked_id);
        if($backup)
        {
            $this->mysql_backup->process(array(
                'action' => 'like',
                'user_id' => $this->user_in_session->getId(),
                'liked_type' => $likedType,
                'liked_id' => $liked_id,
                'score' => $score,
            ));
        }
        $currentStatus = 1;
        return $currentStatus;
    }

    public function unlike($unlikedType, $unliked_id, $backup = true)
    {
        $this->redis->zRem("user:".$this->user_in_session->getId().":likes:".$unlikedType, $unliked_id);
        $this->redis->zRem($unlikedType.":".$unliked_id.":liked:user", $this->user_in_session->getId());
        $this->redis->zIncrBy("ranking:".$unlikedType, -1, $unliked_id);
        if($backup)
        {
            $this->mysql_backup->process(array(
                'action' => 'unlike',
                'user_id' => $this->user_in_session->getId(),
                'liked_type' => $unlikedType,
                'liked_id' => $unliked_id,
                'score' => false,
            ));
        }
        $currentStatus = 0;
        return $currentStatus;
    }

    public function likes($likedType, $liked_id)
    {
        return is_null($this->redis->zScore("user:".$this->user_in_session->getId().":likes:".$likedType, $liked_id)) ? false : true;
    }

    public function getLiked($likedType)
    {
        return $this->redis->zRange("user:".$this->user_in_session->getId().":likes:".$likedType, 0, -1);
    }

    public function getLikers($likedType, $liked_id, $showScores = false, $maxResults = false, $page = 1)
    {
        $offset = $maxResults ? (($page - 1)*$maxResults) : 0;
        $lastItem = $maxResults ? ($offset + ($maxResults - 1)) : -1;

        return $showScores ? $this->redis->zRange($likedType.":".$liked_id.":liked:user", $offset, $lastItem, 'withscores') : array_filter($this->redis->zRange($likedType.":".$liked_id.":liked:user", $offset, $lastItem), function($v){
            return $v != 'undefined';
        });
    }

    public function getLikersByScore($likedType, $liked_id, $min_score, $max_score, $showScores = false, $maxResults = false, $page = 1)
    {
        $offset = $maxResults ? (($page - 1)*$maxResults) : 0;
        $count = $maxResults ? $maxResults : -1;

        return $showScores ? $this->redis->zRangeByScore($likedType.":".$liked_id.":liked:user", $min_score, $max_score, array('withscores' => true, 'limit' => array($offset, $count))) : array_filter($this->redis->zRangeByScore($likedType.":".$liked_id.":liked:user", $offset, $count), function($v){
            return $v != 'undefined';
        });
    }

    public function getTotalLikes($likedType, $liked_id)
    {
        return $this->redis->zCard($likedType.":".$liked_id.":liked:user");
    }

    /* Not tested, might work */
    /*public function getTotalLikesByScore($likedType, $liked_id, $min_score = false, $max_score = false)
    {
        $min_score = $min_score ? $min_score : time();
        $max_score = $max_score ? $max_score : time();
        return $this->redis->zCount($likedType.":".$liked_id.":liked:user", $min_score, $max_score);
    }*/

    public function switchLikeStatus($likedType, $liked_id, $score = false)
    {
        return $this->likes($likedType, $liked_id) ? $this->unlike($likedType, $liked_id) : $this->like($likedType, $liked_id, $score);
    }

    public function getMostLiked($likedType, $maxResults = false, $page = 1, $showScores = false)
    {
        $offset = $maxResults ? (($page - 1)*$maxResults) : 0;
        $lastItem = $maxResults ? ($offset + ($maxResults - 1)) : -1;
        if($showScores)
        {
            return array_filter($this->redis->zRevRange("ranking:".$likedType, $offset, $lastItem, 'withscores'), function($v, $k){
                return $v && $k;
            }, ARRAY_FILTER_USE_BOTH);
        } else {
            return array_filter($this->redis->zRevRange("ranking:".$likedType, $offset, $lastItem), function($v){
                return $v;
            });
        }
    }

    /*public function getMostLikedByScore($likedType, $maxResults = false, $page = 1, $showScores = false, $min_score = false, $max_score = false)
    {
        $offset = $maxResults ? (($page - 1)*$maxResults) : 0;
        $lastItem = $maxResults ? ($offset + ($maxResults - 1)) : -1;
        $min_score = $min_score ? $min_score : time();
        $max_score = $max_score ? $max_score : time();
        if($showScores)
        {
            return array_filter($this->redis->zRevRangeByScore("ranking:".$likedType, $max_score, $min_score, array(
                'withscores' => true,
                'limit' => array($offset, $lastItem)
            )), function($v, $k){
                return $v && $k;
            }, ARRAY_FILTER_USE_BOTH);
        } else {
            return array_filter($this->redis->zRevRangeByScore("ranking:".$likedType, $max_score, $min_score, array(
                'withscores' => false,
                'limit' => array($offset, $lastItem)
            )), function($v){
                return $v;
            });
        }
    }*/
}