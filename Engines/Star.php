<?php

namespace XLabs\LikeBundle\Engines;

use Predis\Client as Predis;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use XLabs\LikeBundle\Queue\Producer as MysqlBackup;
use XLabs\LikeBundle\Entity\User;

class Star
{
    private $config;
    private $redis;
    protected $ip;
    protected $user_in_session;
    private $mysql_backup;

    public function __construct($config, TokenStorageInterface $token_storage, MysqlBackup $mysql_backup, RequestStack $request_stack)
    {
        $this->config = $config;
        $this->redis = new Predis(array(
            'scheme' => 'tcp',
            'host'   => $config['redis_settings']['host'],
            'port'   => $config['redis_settings']['port'],
            'database' => $config['redis_settings']['database_id']
        ), array(
            'prefix' => isset($config['_star_key_namespace']) ? $config['_star_key_namespace'].':' : ''
        ));
        $this->user_in_session = $token_storage->getToken() ? $token_storage->getToken()->getUser() : false;
        $request = $request_stack->getCurrentRequest();
        $this->ip = $request ? $request->getClientIp() : null;
        $this->mysql_backup = $mysql_backup;
    }

    public function disableLogging()
    {
        /*
         * For bulk operations, call this method to avoid PHP memory leaks
         */
        $this->redis->getConnection()->setLogger(null);
        return $this;
    }

    public function getInstance()
    {
        return $this->redis;
    }

    public function getAllKeys($redis_instance = false)
    {
        $redis_instance = $redis_instance ? $redis_instance : $this->redis;
        $keyPattern = "*";
        $keys = $redis_instance->keys($keyPattern);
        return $keys;
    }

    public function deleteAllKeys($redis_instance = false)
    {
        $redis_instance = $redis_instance ? $redis_instance : $this->redis;
        $keys = $this->getAllKeys();
        if($keys)
        {
            $prefix = $redis_instance->getOptions()->__get('prefix')->getPrefix();
            foreach($keys as $key)
            {
                $key = substr($key, strlen($prefix));
                $redis_instance->del(array($key));
            }
        }
    }

    public function changePrefix($old_prefix, $new_prefix)
    {
        $redis['generic'] = new Predis([
            'scheme' => 'tcp',
            'host'   => $this->config['redis_settings']['host'],
            'port'   => $this->config['redis_settings']['port'],
            'database' => $this->config['redis_settings']['database_id']
        ]);
        $keys = $this->getAllKeys($redis['generic']);
        if($keys)
        {
            foreach($keys as $key)
            {
                if(strlen(strstr($key, $old_prefix)) > 0)
                {
                    $redis['generic']->rename($key, str_replace($old_prefix, $new_prefix, $key));
                }
            }
        }
        $redis['generic']->disconnect();
    }

    public function setUser($user)
    {
        if(is_array($user))
        {
            $u = $user;
            $user = new User();
            $user->id = $u['id'];
        }
        $this->user_in_session = $user;
        return $this;
    }

    public function rate($ratedType, $rated_id, $score, $backup = true)
    {
        if($this->getUserRating($ratedType, $rated_id))
        {
            return false;
        }
        //$key = $this->user_in_session ? 'user' : 'ip';
        //$value = $this->user_in_session ? $this->user_in_session->getId() : $this->ip;
        $score = max(0, $score);
        $score = min($this->config['star_rating']['amount'], $score);
        $this->redis->zAdd("user:".$this->user_in_session->getId().":stars:".$ratedType, array($rated_id => $score));
        $this->redis->zAdd($ratedType.":".$rated_id.":starred:user", array($this->user_in_session->getId() => $score));
        $this->redis->zIncrBy("ranking:".$ratedType, $score, $rated_id);
        $this->redis->zAdd("average:".$ratedType, array($rated_id => $this->getAverageRating($ratedType, $rated_id, $score)));
        $this->redis->zIncrBy("rating_count:".$ratedType, 1, $rated_id);
        if($backup)
        {
            $this->mysql_backup->process(array(
                'action' => 'rating',
                'user_id' => $this->user_in_session->getId(),
                'liked_type' => $ratedType,
                'liked_id' => $rated_id,
                'score' => $score,
            ));
        }
        $currentStatus = 1;
        return $currentStatus;
    }

    /*
     * if $new_rating is set, it will return the new average, otherwise will return the current average
     */
    public function getAverageRating($ratedType, $rated_id, $new_rating = false)
    {
        $average = $this->redis->zScore("average:".$ratedType, $rated_id);
        $average = $average ? $average : 0;
        if($new_rating)
        {
            $rating_count = $this->redis->zScore("rating_count:".$ratedType, $rated_id);
            $rating_count = $rating_count ? $rating_count : 0;
            $average = ($average * $rating_count + $new_rating) / ($rating_count + 1);
            $average = ceil($average);
            //$average = ($average > 0) ? ceil(($average + $new_rating) / 2) : $new_rating;
        }
        $average = max(0, $average);
        $average = min($this->config['star_rating']['amount'], $average);
        return (int)$average;
    }

    public function getTotalRatings($ratedType, $rated_id)
    {
        return $this->redis->zCard($ratedType.":".$rated_id.":starred:user");
    }

    public function getUserRating($ratedType, $rated_id)
    {
        $rating = $this->redis->zScore("user:".$this->user_in_session->getId().":stars:".$ratedType, $rated_id);
        if($rating)
        {
            $rating = (int)$rating;
            $rating = max(0, $rating);
            $rating = min($this->config['star_rating']['amount'], $rating);
            return $rating;
        }
        return 0;
    }

    public function getMostRated($ratedType, $maxResults = false, $page = 1, $showScores = false)
    {
        $offset = $maxResults ? (($page - 1)*$maxResults) : 0;
        $lastItem = $maxResults ? ($offset + ($maxResults - 1)) : -1;
        if($showScores)
        {
            return array_filter($this->redis->zRevRange("ranking:".$ratedType, $offset, $lastItem, 'withscores'), function($v, $k){
                return $v && $k;
            }, ARRAY_FILTER_USE_BOTH);
        } else {
            return array_filter($this->redis->zRevRange("ranking:".$ratedType, $offset, $lastItem), function($v){
                return $v;
            });
        }
    }

    public function getMostRatedByUser($ratedType, $maxResults = false, $page = 1, $showScores = false)
    {
        $offset = $maxResults ? (($page - 1)*$maxResults) : 0;
        $lastItem = $maxResults ? ($offset + ($maxResults - 1)) : -1;
        if($showScores)
        {
            return array_filter($this->redis->zRevRange("user:".$this->user_in_session->getId().":stars:".$ratedType, $offset, $lastItem, 'withscores'), function($v, $k){
                return $v && $k;
            }, ARRAY_FILTER_USE_BOTH);
        } else {
            return array_filter($this->redis->zRevRange("user:".$this->user_in_session->getId().":stars:".$ratedType, $offset, $lastItem), function($v){
                return $v;
            });
        }
    }
}