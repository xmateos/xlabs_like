<?php

namespace XLabs\LikeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \DateTime;

/**
 * @ORM\Entity
 * @ORM\Table(name="xlabs_star_ratings", uniqueConstraints={@ORM\UniqueConstraint(name="unique_row",columns={"rated_id", "rated_type", "user_id"})}, indexes={@ORM\Index(name="search_idx", columns={"rated_id", "rated_type", "user_id"})})
 */
class StarRating
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\Column(name="rated_id", type="integer", nullable=false)
     */
    protected $rated_id;

    public function getRatedId()
    {
        return $this->rated_id;
    }

    public function setRatedId($rated_id)
    {
        $this->rated_id = $rated_id;
    }

    /**
     * @ORM\Column(name="rated_type", type="string", length=64, nullable=false)
     */
    protected $rated_type;

    public function getRatedType()
    {
        return $this->rated_type;
    }

    public function setRatedType($rated_type)
    {
        $this->rated_type = $rated_type;
    }

    /**
     * @ORM\Column(name="score", type="integer", nullable=false)
     */
    private $score;

    public function getScore()
    {
        return $this->score;
    }

    public function setScore(DateTime $score)
    {
        $this->score = $score;
    }

    /*
     * @ORM\ManyToOne(targetEntity="XLabs\LikeBundle\Model\XLabsLikeUserInterface")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     **/
    /**
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    protected $user_id;

    public function getUserId()
    {
        return $this->user_id;
    }

    //public function setUser(XLabsLikeUserInterface $user)
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    public function __construct($user_id, $rated_type, $rated_id, $score)
    {
        $this->user_id = $user_id;
        $this->rated_type = $rated_type;
        $this->rated_id = $rated_id;
        $this->score = $score;
    }
}