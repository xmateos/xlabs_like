<?php

namespace XLabs\LikeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \DateTime;
//use XLabs\LikeBundle\Model\XLabsLikeUserInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="xlabs_likes", uniqueConstraints={@ORM\UniqueConstraint(name="unique_row",columns={"liked_id", "liked_type", "user_id"})}, indexes={@ORM\Index(name="search_idx", columns={"liked_id", "liked_type", "user_id"})})
 */
class Like
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\Column(name="liked_id", type="integer", nullable=false)
     */
    protected $liked_id;

    public function getLikedId()
    {
        return $this->liked_id;
    }

    public function setLikedId($liked_id)
    {
        $this->liked_id = $liked_id;
    }

    /**
     * @ORM\Column(name="liked_type", type="string", length=64, nullable=false)
     */
    protected $liked_type;

    public function getLikedType()
    {
        return $this->liked_type;
    }

    public function setLikedType($liked_type)
    {
        $this->liked_type = $liked_type;
    }

    /**
     * @ORM\Column(name="score", type="datetime", nullable=false)
     */
    private $score;

    public function getScore()
    {
        return $this->score;
    }

    public function setScore(DateTime $score)
    {
        $this->score = $score;
    }

    /*
     * @ORM\ManyToOne(targetEntity="XLabs\LikeBundle\Model\XLabsLikeUserInterface")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     **/
    /**
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    protected $user_id;

    public function getUserId()
    {
        return $this->user_id;
    }

    //public function setUser(XLabsLikeUserInterface $user)
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    public function __construct($user_id, $liked_type, $liked_id, $score)
    {
        $this->user_id = $user_id;
        $this->liked_type = $liked_type;
        $this->liked_id = $liked_id;
        $this->score = $score;
    }
}