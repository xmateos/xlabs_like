<?php

namespace XLabs\LikeBundle\Entity;

class User
{
    public $id;

    public function getId()
    {
        return $this->id;
    }

    public function __get($name)
    {
        return $this->$name;
    }
    public function __set($name, $value)
    {
        $this->$name = $value;
    }
}