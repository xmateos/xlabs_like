A redis driven like engine.

## Installation ##

Install through composer:

```bash
php -d memory_limit=-1 composer.phar require xlabs/likebundle
```

This bundle depends on "xlabs/rabbitmqbundle". Make sure to set it up too.

In your AppKernel

```php
public function registerbundles()
{
    return [
    	...
    	...
    	new XLabs\LikeBundle\XLabsLikeBundle(),
    ];
}
```

```bash
php bin/console doctrine:schema:update --force
```

## Routing

Append to main routing file:

``` yml
# app/config/routing.yml
  
xlabs_like_engine:
    resource: "@XLabsLikeBundle/Resources/config/routing.yml"
    #prefix:   /
```

## Configuration sample
Default values are shown below:
``` yml
# app/config/config.yml
  
x_labs_like:
    redis_settings:
        host: 192.168.5.23
        port: 6379
        database_id: 7
    _key_namespace: 'xlabs:like'
    _star_key_namespace: 'xlabs:star'
    backup: # for mysql backup/restore
        <alias>: <entity_FQCN>
    star_rating:
        amount: 5 # amount of stars, 5 is the default

assetic:
    ...
    bundles: [ ..., 'XLabsLikeBundle']
    ...
```

### Usage ###
Append this anywhere in you template
```php
{% include 'XLabsLikeBundle:Like:loader.html.twig' %}
```

To see a sample template, check:
```php
XLabsLikeBundle:Like:example.html.twig
```

<!--
Also, if you have 'resolve_target_entities' set in doctrine´s orm config section, you will need to add the following:
``` yml
# app/config/config.yml

doctrine:
    ...
    orm:
        ...
        resolve_target_entities:
            ...
            XLabs\LikeBundle\Model\XLabsLikeUserInterface: YourBundle\Entity\YourFOSUserExtendedEntity
```
-->

### MySQL Backup ###
Make sure you run the following command. This is the consumer that will save all operations in the project DB.
``` bash
php bin/console xlabs:like:mysql_backup --no-debug
```
If ever Redis lost all the data, you will be able to recover it by issueing the following command:
``` bash
php bin/console xlabs:like:restore --no-debug
```
If you already have data in redis and want to create a mysql backup of it, there´s a sample one-time command you should copy to your project and adapt conveniently:
``` bash
php bin/console xlabs:like:initial_backup --no-debug
```

### Event listener ###
If you want some action to take place whenever a LIKE takes place in the frontends, you can create an event listener as follows:
``` yml
# YourBundle/Resources/config/services.yml
    ...
    xlabs_like.event_listener:
        class:  YourBundle\EventListeners\YourListener.php
        tags:
            - { name: kernel.event_listener, event: xlabs_like.event, method: yourListenerMethod }
```
```php
use Symfony\Component\EventDispatcher\Event;
  
class YourListener extends Event
{
    public function yourListenerMethod(Event $event)
    {
        dump($event->getLike()); die;
    }
}
```
The $event variable contains all info about the LIKE action that has taken place.

### Tweaking ###
By default, the service uses the user in session. If you ever wanted to use the service on your own by using any specific user performing the LIKE action:
```php
$user = $em->getRepository('YourBundle:YourUserEntity')->find(<ID>);
$like_engine = $container->get('xlabs_like_engine');
$like_engine->setUser($user);
$like_engine->...
```