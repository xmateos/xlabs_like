<?php

namespace XLabs\LikeBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class Like extends Event
{
    const NAME = 'xlabs_like.event';

    protected $like;

    public function __construct($like)
    {
        $this->like = $like;
    }

    public function getLike()
    {
        return $this->like;
    }
}