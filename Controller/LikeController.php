<?php

namespace XLabs\LikeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use XLabs\LikeBundle\Event\Like as LikeEvent;

class LikeController extends Controller
{
    /**
     * @Route("/example", name="xlabs_like_example")
     */
    public function exampleAction()
    {
        return $this->render('XLabsLikeBundle:Like:example.html.twig');
    }

    /**
     * @Route("/action", name="xlabs_like_ajax", options={"expose"=true})
     */
    public function likeAction()
    {
        $request = $this->get('request_stack')->getCurrentRequest();

        $data_xlabs_like = stripslashes(file_get_contents("php://input"));
        $data_xlabs_like = json_decode($data_xlabs_like, true);
        $data_xlabs_like = $data_xlabs_like['data-xlabs-like'];

        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $response = array(
            'currentStatus' => 0,
            'currentTotalLikes' => 0,
        );
        $xlabs_like_engine = $this->get('xlabs_like_engine');
        if($request->isMethod('POST') && $data_xlabs_like && !is_string($user))
        {
            $response['currentStatus'] = $xlabs_like_engine->switchLikeStatus($data_xlabs_like['entityType'], $data_xlabs_like['entity_id']);

            // Dispatch event
            $event = new LikeEvent(array(
                'liker_id' => $user->getId(),
                'likedType' => $data_xlabs_like['entityType'],
                'liked_id' => $data_xlabs_like['entity_id'],
                'timestamp' => time(),
                'currentStatus' => $response['currentStatus']
            ));
            $this->get('event_dispatcher')->dispatch(LikeEvent::NAME, $event);
        }
        $response['currentTotalLikes'] = $xlabs_like_engine->getTotalLikes($data_xlabs_like['entityType'], $data_xlabs_like['entity_id']);

        $response = new Response(json_encode($response, JSON_PRETTY_PRINT), 200);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/star/action", name="xlabs_star_ajax", options={"expose"=true})
     */
    public function starAction()
    {
        $request = $this->get('request_stack')->getCurrentRequest();

        $data_xlabs_star = stripslashes(file_get_contents("php://input"));
        $data_xlabs_star = json_decode($data_xlabs_star, true);
        $data_xlabs_star = $data_xlabs_star['data-xlabs-star'];
        //dump($data_xlabs_star); die;

        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $response = array(
            'currentStatus' => 0,
            'currentTotalLikes' => 0,
        );
        $xlabs_star_engine = $this->get('xlabs_star_engine');
        if($request->isMethod('POST') && $data_xlabs_star && !is_string($user))
        {
            //dump($xlabs_star_engine->getAverageRating($data_xlabs_star['entityType'], $data_xlabs_star['entity_id'])); die;
            $response['currentStatus'] = $xlabs_star_engine->rate($data_xlabs_star['entityType'], $data_xlabs_star['entity_id'], $data_xlabs_star['rating']);

            // Dispatch event
            /*$event = new LikeEvent(array(
                'liker_id' => $user->getId(),
                'likedType' => $data_xlabs_like['entityType'],
                'liked_id' => $data_xlabs_like['entity_id'],
                'timestamp' => time(),
                'currentStatus' => $response['currentStatus']
            ));
            $this->get('event_dispatcher')->dispatch(LikeEvent::NAME, $event);*/
        }
        //$response['currentTotalLikes'] = $xlabs_like_engine->getTotalLikes($data_xlabs_like['entityType'], $data_xlabs_like['entity_id']);

        $response = new Response(json_encode($response, JSON_PRETTY_PRINT), 200);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
}
